<?php

namespace App\Helpers;

use Closure;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Exception;
use Illuminate\Http\Request;

class ChunksUploader
{
    private $maxFileAge = 600; //sec

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getTempPath()
    {
        $path = storage_path() . '/temp';
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        return $path;
    }

    public function getPath()
    {
        $path = public_path() . '/video';
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        return $path;
    }

    public function receiveSingle($name, Closure $handler)
    {
        return $handler($this->request->file($name));
    }

    private function appendData($filePathPartial, UploadedFile $file)
    {
        if (!$out = @fopen($filePathPartial, 'ab')) {
            throw new Exception('Failed to open output stream.');
        }

        if (!$in = @fopen($file->getPathname(), 'rb')) {
            throw new Exception('Failed to open input stream');
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);
    }

    public function receiveChunks($name, Closure $handler)
    {
        $result = false;
        if ($this->request->file($name)) {
            $file         = $this->request->file($name);
            $chunk        = (int)$this->request->get('chunk', false);
            $chunks       = (int)$this->request->get('chunks', false);
            $originalName = $this->request->get('name');

            $filePath = $this->getTempPath() . '/' . $originalName . '.part';
            $this->removeOldData($filePath);

            $this->appendData($filePath, $file);

            if ($chunk == $chunks - 1) {
                $file   = new UploadedFile($filePath, $originalName, 'blob', sizeof($filePath), UPLOAD_ERR_OK, true);
                $result = $handler($file);
                @unlink($filePath);
            }
        }

        return $result;
    }

    public function removeOldData($filePath)
    {
        if (file_exists($filePath) && filemtime($filePath) < time() - $this->maxFileAge) {
            @unlink($filePath);
        }
    }

    public function hasChunks()
    {
        return (bool)$this->request->get('chunks', false);
    }

    public function receive($name, Closure $handler)
    {
        $response = [];

        if ($this->hasChunks()) {
            $result = $this->receiveChunks($name, $handler);
        } else {
            $result = $this->receiveSingle($name, $handler);
        }

        $response['result'] = $result;

        return $response;
    }
}
