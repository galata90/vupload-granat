<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use FFMpeg\FFMpeg;
use FFMpeg\Format\Video\WebM as FormatWebM;
use App\Helpers\ChunksUploader;


class UploadController extends Controller
{
    protected $ffmpeg;

    protected $chunksUploader;

    public function __construct(ChunksUploader $chunksUploader)
    {
        $this->ffmpeg         = FFMpeg::create();
        $this->chunksUploader = $chunksUploader;
    }

    public function upload(Request $request)
    {
        return $this->chunksUploader->receive('file', function ($file) {
            $uploadedFile = $file->move($this->chunksUploader->getPath() . '/',
                $file->getClientOriginalName());

            $video = $this->ffmpeg->open($uploadedFile);
            $video->save(new FormatWebM(),
                $this->chunksUploader->getPath() . '/' . uniqid('', true) . '.webm');

            @unlink($uploadedFile);

            return 'ready';
        });
    }

}
